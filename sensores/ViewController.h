//
//  ViewController.h
//  sensores
//
//  Created by jose garcia on 24/08/13.
//  Copyright (c) 2013 jose garcia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
@interface ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,
CBCentralManagerDelegate,CBPeripheralDelegate>{
    NSMutableArray *listaDispositivos;

}
@property (strong,nonatomic) CBCentralManager *manager;
@property (unsafe_unretained, nonatomic) IBOutlet UITableView *listadoDispositivos;
- (IBAction)clickBTN:(id)sender;


@end
